package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

var (
	target int
	min    int
	max    int
)

func main() {
	min = 1
	max = 100

	rand.Seed(time.Now().Unix())
	target = rand.Intn(100)

	start()
}

func start() {
	var in int
	for {
		fmt.Print("range between [", min, max, "], input your number: ")
		fmt.Scanln(&in)
		status(in)
	}
}

func status(in int) {
	// 输入数字超出边界
	if in < min || in > max {
		fmt.Println("invalid input number!!")
		return
	}

	// 数字炸弹爆炸
	if in == target {
		fmt.Println("====Bomb!====")
		pauseAndExit()
	}

	// 更新边界
	if in < target {
		min = in
	} else {
		max = in
	}

	// 游戏胜利
	if max-min == 2 {
		fmt.Println("====You WIN!====")
		pauseAndExit()
	}
}

func pauseAndExit() {
	fmt.Println("GAME OVER...")
	var in int
	fmt.Scanln(&in)
	os.Exit(0)
}
